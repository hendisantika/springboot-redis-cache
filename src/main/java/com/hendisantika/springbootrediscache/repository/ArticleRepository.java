package com.hendisantika.springbootrediscache.repository;

import com.hendisantika.springbootrediscache.entity.Article;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-redis-cache
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/11/19
 * Time: 07.06
 */
public interface ArticleRepository extends CrudRepository<Article, Long> {
}