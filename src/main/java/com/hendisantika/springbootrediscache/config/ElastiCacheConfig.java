package com.hendisantika.springbootrediscache.config;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-redis-cache
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/11/19
 * Time: 07.08
 */
public class ElastiCacheConfig {
    private static final String DEFAULT_CLUSTER = "false";
    @Value("${spring.redis.host}")
    private String host;
    @Value("${spring.redis.port}")
    private Integer port;
    @Value("${spring.redis.lettuce.pool.max-idle}")
    private String maxIdle;
    @Value("${spring.redis.lettuce.pool.min-idle}")
    private String minIdle;
    @Value("${spring.redis.lettuce.pool.max-wait}")
    private String maxWait;

    @Bean("dynamicCacheService")
    ElastiCacheService testDynamicCacheService() {
        return ElastiCacheService.getInstance("hmscloud-dyn-cache.5evooa.clustercfg.euw1.cache.amazonaws.com", 6379, poolConfig());
    }

    @Bean
    GenericObjectPoolConfig poolConfig() {

        GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();

        poolConfig.setMaxIdle(
                Integer.parseInt(maxIdle));
        poolConfig.setMinIdle(
                Integer.parseInt(minIdle));
        poolConfig.setMaxWaitMillis(Long
                .parseLong(maxWait));

        return poolConfig;

    }

}
