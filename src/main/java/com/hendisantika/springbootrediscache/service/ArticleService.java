package com.hendisantika.springbootrediscache.service;

import com.hendisantika.springbootrediscache.entity.Article;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-redis-cache
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/11/19
 * Time: 07.07
 */
public interface ArticleService {
    List<Article> getAllArticles();

    Article getArticleById(long articleId);

    Article addArticle(Article article);

    Article updateArticle(Article article);

    void deleteArticle(long articleId);
}
